package com.core


import com.tranformations.FaultyInitialize

@FaultyInitialize(attributeX = 'digitoX', attributeY = 'digitoY')
class Operacion {
    int digitoX
    int digitoY

    Operacion(int digitoX = 60, int digitoY = 10) {
        this.digitoX = digitoX
        this.digitoY = digitoY
    }

    int Division()
    {
        int total = digitoX / digitoY
        return total
    }
}
