package com.processors

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.AbstractASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

import java.lang.reflect.Modifier

@GroovyASTTransformation(phase = CompilePhase.INSTRUCTION_SELECTION)
class FaultInjectionDivision extends AbstractASTTransformation{
    @Override
    void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        AnnotationNode annotation = (AnnotationNode) astNodes[0]
        ClassNode classNode = (ClassNode) astNodes[1]

        String fieldNameX = ((ConstantExpression) annotation.getMember('attributeX')).value
        String fieldNameY = ((ConstantExpression) annotation.getMember('attributeY')).value

        Expression thisKeyword = new VariableExpression('this')
        Expression propertyNameX = new ConstantExpression(fieldNameX)
        Expression propertyNameY = new ConstantExpression(fieldNameY)

        Expression setPropertyExpression = new MethodCallExpression(
                new PropertyExpression(
                        thisKeyword,'metaClass'
                ),
                'setProperty',
                new ArgumentListExpression([thisKeyword, propertyNameX, new ConstantExpression(0)], [thisKeyword, propertyNameY, new ConstantExpression(0)])
        )

        Statement methodBody = new ExpressionStatement(
                setPropertyExpression
        )

        MethodNode method = new MethodNode(
                'setProperty',
                Modifier.PUBLIC,
                ClassHelper.VOID_TYPE,
                [new Parameter(ClassHelper.STRING_TYPE, 'name'),
                 new Parameter(ClassHelper.OBJECT_TYPE, 'value')] as Parameter[],
                [] as ClassNode[],
                methodBody
        )
        classNode.addMethod(method)
    }
}
