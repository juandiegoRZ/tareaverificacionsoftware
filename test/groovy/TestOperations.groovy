import com.core.Operacion
import com.processors.FaultInjectionDivision
import org.testng.annotations.Test

import static org.hamcrest.CoreMatchers.equalTo
import static org.hamcrest.MatcherAssert.assertThat

class TestOperations {

    @Test
    void testTarea() {
        Operacion op = new Operacion(40,5)
        int test = op.Division()
        assertThat(test, equalTo(6))
        //FaultInjectionDivision fl = new FaultInjectionDivision()
        //fl.metaClass.setProperty()
        //assertThat(op.digitoX, equalTo(60))
        //assertThat(op.digitoY, equalTo(10))
    }
}
